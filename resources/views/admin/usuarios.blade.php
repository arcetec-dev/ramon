<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Usuario') }}
        </h2>
    </x-slot>

    @livewire('usercomponent')
</x-app-layout>
