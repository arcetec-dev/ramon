<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'id'    => 1,
                'title' => 'user_access',
            ],
            [
                'id'    => 2,
                'title' => 'role_access',
            ],
            [
                'id'    => 3,
                'title' => 'prmission_access',
            ],
            [
                'id'    => 4,
                'title' => 'country_access',
            ],
        ];

        Permission::insert($records);
    }
}
