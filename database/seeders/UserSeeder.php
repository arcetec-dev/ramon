<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'id'    => 1,
                'name' => 'SuperAdmin',
                'email' => 'super@email.com',
                'password'       => bcrypt('password'),
                'remember_token' => null,
                'email_verified_at' => now(),
            ],
            [
                'id'    => 2,
                'name' => 'Admin',
                'email' => 'admin@email.com',
                'password'       => bcrypt('password'),
                'remember_token' => null,
                'email_verified_at' => now(),
            ],
            [
                'id'    => 3,
                'name' => 'Usuario',
                'email' => 'user@email.com',
                'password'       => bcrypt('password'),
                'remember_token' => null,
                'email_verified_at' => now(),
            ],
        ];

        User::insert($records);
    }
}
